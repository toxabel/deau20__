/* Create one function that reports all information for a particular client and timeframe:
	� Customer's name, surname and email address;
	� Number of films rented during specified timeframe;
	� Comma-separated list of rented films at the end of specified time period;
	� Total number of payments made during specified time period;
	� Total amount paid during specified time period;
	Function's input arguments: client_id, left_boundary, right_boundary.
	The function must analyze specified timeframe [left_boundary, right_boundary] and output specified information for this timeframe.
	Function's result format: table with 2 columns �metric_name� and �metric_value�.*/



CREATE OR REPLACE FUNCTION npublic.get_metric_name_value 
	(
		  client_id INT4
		, left_boundary DATE
		, right_boundary DATE
	)
RETURNS TABLE 
(
	  metric_name TEXT
	, metric_value TEXT
		
) AS 
$$

BEGIN
	
RETURN QUERY
SELECT 
	  rez.metric_name
	, rez.metric_value
FROM (	
	SELECT 
		  1 AS SORT_ID	
		, 'customer''s info'::TEXT AS metric_name
		, CONCAT(c.first_name, ' ', c.last_name, ' ', c.email)::TEXT AS metric_value
	FROM customer c
	WHERE c.customer_id = client_id
	
	UNION
	
	SELECT
		  2 AS SORT_ID	
		, 'num. of films rented'::TEXT AS metric_name
		, COUNT(*)::TEXT as metric_value
	FROM rental r 
	WHERE r.customer_id = client_id
		AND r.rental_date BETWEEN left_boundary AND right_boundary
	
	UNION	

	SELECT
		  3 AS SORT_ID	
		, 'rented films'' titles'::TEXT AS metric_name
		, STRING_AGG (f.title, ', ')::TEXT as metric_value
	FROM rental r2
	JOIN inventory i
		ON r2.inventory_id = i.inventory_id 
	JOIN film f
		ON i.film_id = f.film_id 
	WHERE r2.customer_id = client_id
		AND r2.rental_date BETWEEN left_boundary AND right_boundary
	
	UNION	
	
	SELECT
		  4 AS SORT_ID	
		, 'num. of payments'::TEXT AS metric_name
		, COUNT (*)::TEXT as metric_value
	FROM payment p 
	WHERE p.customer_id = client_id
		AND p.payment_date BETWEEN left_boundary AND right_boundary

	UNION	
		
	SELECT 
		  5 AS SORT_ID	
		, 'payments'' amount'::TEXT AS metric_name
		, SUM (p2.amount)::TEXT as metric_value
	FROM payment p2 	
	WHERE p2.customer_id = client_id
		AND p2.payment_date BETWEEN left_boundary AND right_boundary
) AS rez
ORDER BY SORT_ID;	
END;
$$
LANGUAGE plpgsql;




--SELECT * FROM npublic.get_metric_name_value (80, '2017-01-01 23:05:21', '2017-12-31 01:43:41')
	

	

	

