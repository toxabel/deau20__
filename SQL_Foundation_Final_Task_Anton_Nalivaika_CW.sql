/* Fuel station network domain */

/* Task 1. Create conceptual and logical models (3nf) for given domain (save it as PNG files). Describe business process (process of how business adds new data to this database). 
           It is expected that your database will populate over time, so your transaction table needs to have "timestamp of transaction" field. */


/* Ans:	   The system is designed for continuous fuel sales:
		   1) Service of fuel equipment of the fuel station network - after performing service work, staff enters data into the system (in the table equipment_service);
		   2) Scheduled control of the minimum fuel level in the tanks - every hour the sensors in the tanks receive a new value and transfer into the system (in the table fuel_volume). 
		   All other information is entered into the system (all remaining tables) when opening a new fuel station or technical changes at the fuel station or fuel equipment. */



/* Task 2. Create physical database design (DDL scripts for tables). Make sure your database is in 3NF and has meaningful keys and constraints. 
 		   Use ALTER TABLE to add constraints (except NOT NULL, UNIQUE and keys). Give meaningful names to your CHECK constraints. Use DEFAULT and STORED AS where appropriate. */


CREATE DATABASE "fuel_db";

CREATE SCHEMA "fuel_sch";


CREATE TABLE fuel_sch.fuel_station (
  "fuel_station_id" SERIAL PRIMARY KEY,
  "name" TEXT NOT NULL,
  "address" TEXT NOT NULL,
  "start_effective_date" DATE NOT NULL DEFAULT now(),
  "end_effective_date" DATE
);

CREATE TABLE fuel_sch.contacts (
  "contacts_id" SERIAL PRIMARY KEY,
  "fuel_station_id" INT4 NOT NULL REFERENCES fuel_sch.fuel_station,
  "email" TEXT,
  "phone" TEXT NOT NULL
);

CREATE TABLE fuel_sch.fuel_type (
  "fuel_type_id" SERIAL PRIMARY KEY,
  "name" TEXT NOT NULL
);

CREATE TABLE fuel_sch.service_type (
  "service_type_id" SERIAL PRIMARY KEY,
  "name" TEXT  NOT NULL,
  "frequency" INT2 NOT NULL
);

CREATE TABLE fuel_sch.tank (
  "tank_id" SERIAL PRIMARY KEY,
  "fuel_station_id" INT4 NOT NULL REFERENCES fuel_sch.fuel_station,
  "fuel_type_id" INT4 NOT NULL REFERENCES fuel_sch.fuel_type,
  "volume" INT4 NOT NULL,
  "min_stock" NUMERIC(5,2) NOT NULL,
  "start_effective_date" DATE NOT NULL DEFAULT now(),
  "end_effective_date" DATE
);

CREATE TABLE fuel_sch.dispenser (
  "dispenser_id" SERIAL PRIMARY KEY,
  "tank_id" INT4 NOT NULL REFERENCES fuel_sch.tank,
  "dnumber" INT2 NOT NULL,
  "start_effective_date" DATE NOT NULL DEFAULT now(),
  "end_effective_date" DATE
);

CREATE TABLE fuel_sch.fuel_volume (
  "fuel_volume_id" SERIAL PRIMARY KEY,
  "tank_id" INT4 NOT NULL REFERENCES fuel_sch.tank,
  "volume" INT4 NOT NULL,
  "datetime" TIMESTAMP NOT NULL
);

CREATE TABLE fuel_sch.equipment_service (
  "equipment_service_id" SERIAL PRIMARY KEY,
  "service_type_id" INT4 NOT NULL REFERENCES fuel_sch.service_type,
  "tank_id" INT4 REFERENCES fuel_sch.tank,
  "dispenser_id" INT4 REFERENCES fuel_sch.dispenser,
  "datetime" TIMESTAMP NOT NULL
);

ALTER TABLE fuel_sch.contacts 
ADD CONSTRAINT chack_email CHECK (email LIKE '%@%');



/* Task 3. Add fictional data to your database (5+ rows per table, 50+ rows total across all tables). Save your data as DML scripts. 
 		   Make sure your surrogate keys' values are not included in DML scripts (they should be created runtime by the database, as well as DEFAULT values where appropriate). 
 		   DML scripts must successfully pass all previously created constraints. */


INSERT INTO fuel_sch.fuel_station (name, address)
VALUES
	 ('Station_1', 'Ap #803-3962 Donec Avenue')
	,('Station_2', '9258 Neque Rd.')
	,('Station_3', '2808 Nec, Ave')
	,('Station_4', 'Ap #867-6871 Aliquet, St.')
	,('Station_5', 'Ap #737-985 Posuere, Ave')
	,('Station_6', '151-8768 Risus. Rd.');

INSERT INTO fuel_sch.contacts (fuel_station_id, email, phone)
VALUES
	 (1, 'nec@ridiculusmus.com','+375-29-204-0114')
	,(2, 'aliquet.libero.Integer@veliteusem.net','+375-29-626-2264')
	,(3, 'molestie.tellus.Aenean@etmalesuadafames.ca','+375-29-141-2794')
	,(4, 'adipiscing@Morbiquisurna.com','+375-29-882-3140')
	,(5, 'interdum.libero.dui@necmollis.com','+375-29-204-3669')
	,(6, 'metus.In@sedliberoProin.com','+375-29-878-9941');

INSERT INTO fuel_sch.fuel_type (name)
VALUES
	 ('92')
	,('95')
	,('98')
	,('100')
	,('diesel')
	,('winter diesel');

INSERT INTO fuel_sch.service_type (name, frequency)
VALUES
	 ('�leaning', 7)
	,('General cleaning', 30)
	,('Adjustment', 60)
	,('Revise', 1)
	,('Lubrication', 90)
	,('Ultrasonic testing', 180);

INSERT INTO fuel_sch.tank (fuel_station_id, fuel_type_id, volume, min_stock)
VALUES
	 (1, 1, 100000, 15)
	,(1, 2, 100000, 15)
	,(1, 5, 100000, 15)
	,(2, 1, 150000, 20)
	,(2, 2, 150000, 20)
	,(2, 5, 150000, 20);

INSERT INTO fuel_sch.dispenser (tank_id, dnumber)
VALUES
 	 (1, 1)
	,(2, 2)
	,(3, 3)
	,(4, 1)
	,(5, 2)
	,(6, 3);

INSERT INTO fuel_sch.fuel_volume (tank_id, volume, datetime)
VALUES
	 (1, 80000, '20-10-17 09:00:00')
	,(1, 79500, '20-10-17 10:00:00')
	,(1, 78000, '20-10-17 11:00:00')
	,(2, 50000, '20-10-17 09:00:00')
	,(2, 48500, '20-10-17 10:00:00')
	,(2, 46000, '20-10-17 11:00:00');

INSERT INTO fuel_sch.equipment_service (service_type_id, tank_id, dispenser_id, datetime)
VALUES
 	 (4, 1, NULL, '20-10-17 08:00:00')
	,(4, 2, NULL, '20-10-17 08:00:00')
	,(4, 3, NULL, '20-10-17 08:00:00')
	,(4, NULL, 1, '20-10-17 08:00:00')
	,(4, NULL, 2, '20-10-17 08:00:00')
	,(4, NULL, 3, '20-10-17 08:00:00');

COMMIT



/* Task 4.1 Create function that UPDATEs data in one of your tables (input arguments: table's primary key value, column name and column value to UPDATE to). */


--DROP FUNCTION fuel_sch.update_fuel_type_value

CREATE OR REPLACE FUNCTION fuel_sch.update_fuel_type_value 
	(
		  pkey_value INT4
		, c_name TEXT
		, c_value TEXT
	)
RETURNS SETOF fuel_sch.fuel_type
LANGUAGE plpgsql
SECURITY DEFINER
AS $$

DECLARE
	tmpSQL TEXT;

BEGIN

tmpSQL :=
   'UPDATE fuel_sch.fuel_type
	SET 
		'|| c_name || ' = '''|| c_value || '''
	WHERE fuel_type_id = '|| pkey_value ;
	
EXECUTE tmpSQL;

RETURN;
END;
$$;

-- SELECT * FROM fuel_sch.update_fuel_type_value (4, 'name', 'i100')



/* Task 4.2 Create function that adds new transaction to your transaction table. Come up with input arguments and output format yourself. 
 			Make sure all transaction attributes can be set with the function (via their natural keys). */


CREATE OR REPLACE FUNCTION fuel_sch.add_fuel_volume_value
(
	 tank_id INT4
  	,volume INT4
  	,datetime TIMESTAMP
)
RETURNS INT4 AS
$$	

DECLARE 
    id_out INT4;  
BEGIN  
	
	INSERT INTO fuel_volume (tank_id, volume, datetime)
	VALUES
		($1, $2, $3 )
	RETURNING fuel_volume.fuel_volume_id INTO id_out;
	RETURN id_out;

END
$$ 
LANGUAGE plpgsql;

-- SELECT * FROM fuel_sch.add_fuel_volume_value (3, 65000, '20-10-17 09:00:00')




/* Task 5. Create view that joins all tables in your database and represents data in denormalized form for the past month. 
 		   Make sure to omit meaningless fields in the result (e.g. surrogate keys, duplicate fields, etc.). */

/* Ans:	    Used FULL JOIN to display all data 
 			But to display more business value (data from the equipment service fact table), better use LEFT JOIN */

CREATE VIEW all_tables_value AS 
SELECT
	  st.name AS service_name
	 ,st.frequency AS service_frequency
	 ,t.tank_id AS tank_name
	 ,d.dispenser_id AS dispenser_name
	 ,es.datetime AS service_time
	 ,fst.name AS fuel_station_name
	 ,fst.address AS fuel_station_address
	 ,c.email AS fuel_station_email
	 ,c.phone AS fuel_station_phone
	 ,fst.start_effective_date AS fuel_station_opening_date
	 ,fst.end_effective_date AS fuel_station_closing_date 
	 ,ft.name AS fuel_type_name
	 ,t.volume AS tank_volume
	 ,t.min_stock AS tank_min_stock
	 ,t.start_effective_date AS tank_start_of_operation
	 ,t.end_effective_date AS tank_end_of_operation
	 ,d.dnumber AS dispenser_number
	 ,d.start_effective_date AS dispenser_start_of_operation
	 ,d.end_effective_date AS dispenser_end_of_operation
FROM fuel_sch.equipment_service es
FULL JOIN fuel_sch.service_type st
	ON es.service_type_id = st.service_type_id
FULL JOIN tank t 
	ON es.tank_id = t.tank_id 
FULL JOIN dispenser d 
	ON es.dispenser_id = d.dispenser_id 
FULL JOIN fuel_type ft
	ON t.fuel_type_id = ft.fuel_type_id 
FULL JOIN fuel_station fst
	ON t.fuel_station_id = fst.fuel_station_id 
FULL JOIN contacts c 
	ON fst.fuel_station_id = c.fuel_station_id 
ORDER BY st.name;

-- SELECT * FROM all_tables_value


/* Task 6. Create manager's read-only role. Make sure he can only SELECT from tables in your database. Make sure he can LOGIN as well. 
 		   Make sure you follow database security best practices when creating role(s). */

-- RESET ROLE;
-- SELECT SESSION_USER, CURRENT_USER;


REVOKE ALL ON DATABASE fuel_db FROM public;
REVOKE ALL ON SCHEMA fuel_sch FROM public;
REVOKE ALL ON ALL TABLES IN SCHEMA fuel_sch from public;


CREATE ROLE manager;													
GRANT USAGE ON SCHEMA fuel_sch TO manager;
GRANT SELECT ON ALL TABLES IN SCHEMA fuel_sch TO manager;

CREATE ROLE manager_CHARLI_PUTT WITH LOGIN PASSWORD '777' IN ROLE manager;


-- SET ROLE manager_CHARLI_PUTT;
-- SELECT * FROM fuel_volume;
-- SELECT fuel_sch.add_fuel_volume_value (3, 64000, '20-10-17 10:00:00');
-- SELECT * FROM all_tables_value

