/* 2. Implement role-based authentication model for dvd_rental database:
		� Create group roles: DB developer, backend tester (read-only), customer (read-only for film and actor)
		� Create personalized role for any customer already existing in the dvd_rental database. Role name must be client_{first_name}_{last_name} (omit curly brackets). 
		  Customer's payment and rental history must not be empty.
		� Assign proper privileges to each role.
		� Verify that all roles are working as intended */

-- RESET ROLE;
-- SELECT SESSION_USER, CURRENT_USER;



REVOKE ALL ON DATABASE postgres FROM public;
REVOKE ALL ON SCHEMA npublic FROM public;
REVOKE ALL ON ALL TABLES IN SCHEMA npublic from public;


CREATE ROLE DB_developer;													-- DB_developer
GRANT USAGE ON SCHEMA npublic TO DB_developer;
GRANT SELECT ON ALL TABLES IN SCHEMA npublic TO DB_developer;
GRANT INSERT ON ALL TABLES IN SCHEMA npublic TO DB_developer;
GRANT UPDATE ON ALL TABLES IN SCHEMA npublic TO DB_developer;


CREATE ROLE backend_tester;													-- backend_tester
GRANT USAGE ON SCHEMA npublic TO backend_tester;
GRANT SELECT ON ALL TABLES IN SCHEMA npublic TO backend_tester;

CREATE ROLE customer;														-- customer
GRANT USAGE ON SCHEMA npublic TO customer;
GRANT SELECT ON film TO customer;
GRANT SELECT ON actor TO customer;

CREATE ROLE client_ROBIN_HAYES WITH LOGIN PASSWORD '555' IN ROLE customer;	-- client_ROBIN_HAYES



-- SET ROLE client_ROBIN_HAYES;
-- SELECT * FROM actor;


