/* Prepare sql queries to resolve each of these questions. Visualize the results using any tool (e.g. Microsoft Excel). */

--Ans:
--1. Profit by sales channel - Compare the value of profit for each sales channel

SELECT
	 c2.channel_desc AS channel_name	
	,SUM (p.unit_price - p.unit_cost)::INT AS profit_amount
FROM profits p 
JOIN channels c2 
	ON p.channel_id = c2.channel_id 
GROUP BY c2.channel_desc
ORDER BY profit_amount DESC



--2. Profit by type of promotions - Compare profit values for all previously held promotions

SELECT 
	 p2.promo_name
	 ,SUM (p.unit_price - p.unit_cost)::INT AS profit_amount
FROM profits p
JOIN promotions p2 
	ON p.promo_id = p2.promo_id 
GROUP BY p2.promo_name 
ORDER BY profit_amount DESC



--3. Profit for various products - Compare profit values for all previously sold products.

SELECT 
	 p2.prod_name
	,SUM (p.unit_price - p.unit_cost)::INT AS profit_amount 
FROM profits p 
JOIN products p2 
	ON p.prod_id = p2.prod_id 
GROUP BY p2.prod_name
ORDER BY profit_amount DESC

