Formulate three the most valuable business questions (from your point of view) which could be resolved with information from Sales History.

Ans:
	
	1. Profit by sales channel - Compare the value of profit for each sales channel;
	
	2. Profit by type of promotions - Compare profit values for all previously held promotions;
	
	3. Profit for various products - Compare profit values for all previously sold products.
	
	