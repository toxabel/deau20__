-- Part 1 

CREATE DATABASE "medical_db";

CREATE SCHEMA "medical_sch";

CREATE TABLE medical_sch.contacts (
  "contacts_id" SERIAL PRIMARY KEY,
  "email" TEXT CHECK (email LIKE '%@%'),
  "phone" TEXT
);

CREATE TABLE medical_sch.location_type (
  "location_type_id" SERIAL PRIMARY KEY,
  "name" TEXT NOT NULL
);

CREATE TABLE medical_sch.medical_institution_type (
  "medical_institution_type_id" SERIAL PRIMARY KEY,
  "name" TEXT NOT NULL
);

CREATE TABLE medical_sch.position_type (
  "position_type_id" SERIAL PRIMARY KEY,
  "name" TEXT NOT NULL
);

CREATE TABLE medical_sch.medical_institution (
  "medical_institution_id" SERIAL PRIMARY KEY,
  "name" TEXT NOT NULL,
  "contacts_id" INT4 NOT NULL REFERENCES medical_sch.contacts,
  "medical_institution_type_id" INT4 NOT NULL REFERENCES medical_sch.medical_institution_type,
  "hospital_beds_number" INT2,
  "daily_patients_examination" INT2 NOT NULL,
  "doctors_quantity" INT2,
  "paramedics_quantity" INT2,
  "nurses_quantity" INT2,
  "start_effective_date" DATE NOT NULL DEFAULT now(),
  "end_effective_date" DATE
);

CREATE TABLE medical_sch.patient (
  "patient_id" SERIAL PRIMARY KEY,
  "name" TEXT NOT NULL,
  "surname" TEXT NOT NULL,
  "date_of_birth" DATE NOT NULL,
  "sex" TEXT NOT NULL CHECK (sex IN ('M', 'W')),
  "contacts_id" INT4 REFERENCES medical_sch.contacts
);

CREATE TABLE medical_sch.address (
  "address_id" SERIAL PRIMARY KEY,
  "address" TEXT NOT NULL,
  "medical_institution_id" INT4 REFERENCES medical_sch.medical_institution,
  "patient_id" INT4 REFERENCES medical_sch.patient
);

CREATE TABLE medical_sch.location (
  "location_id" SERIAL PRIMARY KEY,
  "name" TEXT,
  "address_id" INT4 NOT NULL REFERENCES medical_sch.address,
  "location_type_id" INT4 NOT NULL REFERENCES medical_sch.location_type
);

CREATE TABLE medical_sch.position (
  "position_id" SERIAL PRIMARY KEY,
  "name" TEXT NOT NULL,
  "position_type_id" INT4 NOT NULL REFERENCES medical_sch.position_type
);

CREATE TABLE medical_sch.staff (
  "staff_id" SERIAL PRIMARY KEY,
  "name" TEXT NOT NULL,
  "surname" TEXT NOT NULL,
  "identity_document_number" TEXT NOT NULL,
  "medical_institution_id" INT4 NOT NULL REFERENCES medical_sch.medical_institution,
  "position_id" INT4 NOT NULL REFERENCES medical_sch.position
);

CREATE TABLE medical_sch.patient_visit (
  "patient_visit_id" SERIAL PRIMARY KEY,
  "location_id" INT4 NOT NULL REFERENCES medical_sch.location,
  "patient_id" INT4 NOT NULL REFERENCES medical_sch.patient,
  "staff_id" INT4 NOT NULL REFERENCES medical_sch.staff,
  "start_date_time" TIMESTAMP NOT NULL DEFAULT now(),
  "end_date_time" TIMESTAMP CHECK (end_date_time > start_date_time)
);



INSERT INTO medical_sch.contacts (email, phone)
VALUES 
	 ('nec@ridiculusmus.com','+375-29-204-0114')
	,('aliquet.libero.Integer@veliteusem.net','+375-29-626-2264')
	,('molestie.tellus.Aenean@etmalesuadafames.ca','+375-29-141-2794')
	,('adipiscing@Morbiquisurna.com','+375-29-882-3140')
	,('interdum.libero.dui@necmollis.com','+375-29-204-3669')
	,('metus.In@sedliberoProin.com','+375-29-878-9941')
	,('tempor@Suspendissealiquetsem.org','+375-29-432-6096')
	,('eu@aodiosemper.edu','+375-29-954-3605')
	,('orci.adipiscing@ipsumdolor.co.uk','+375-29-951-6077')
	,('Nulla.eu@pharetrautpharetra.co.uk','+375-29-118-8289')
	,('dolor.dapibus@ametrisusDonec.ca','+375-29-957-9840')
	,('Phasellus.fermentum.convallis@musProin.org','+375-29-102-9904')
	,('Sed.nec@montesnasceturridiculus.com','+375-29-602-0089')
	,('Integer@Pellentesque.net','+375-29-912-4762')
	,('lobortis.Class@Etiambibendum.ca','+375-29-775-8861')
	,('consequat.nec@adipiscing.net','+375-29-277-6808')
	,('est.Nunc.laoreet@Mauris.net','+375-29-741-5678')
	,('sem.semper@egestas.ca','+375-29-567-1046')
	,('aliquet@nisi.org','+375-29-404-0036')
	,('lorem.fringilla.ornare@nuncsedpede.net','+375-29-344-1210');

INSERT INTO medical_sch.location_type (name)
VALUES 
	 ('administrative office')
	,('technical room')
	,('laboratory')
	,('staff room')
	,('medical office')
	,('medical ward')
	,('operationg room')
	,('client''s accommodation')
	,('other location');

INSERT INTO medical_sch.medical_institution_type (name)
VALUES
	 ('Ambulant clinic')
	,('Polyclinic')
	,('Hospital')
	,('Private clinic')
	,('Maternity hospital')
	,('Sanatorium');

INSERT INTO medical_sch.position_type (name)
VALUES
	 ('medical staff')
	,('other staff');

INSERT INTO medical_sch.medical_institution (name, contacts_id, medical_institution_type_id, hospital_beds_number, daily_patients_examination, doctors_quantity, paramedics_quantity, nurses_quantity)
VALUES
	 ('Hospital �5', 1, 3, 350, 200, 40, 20, 40)
	,('Hospital �3', 2, 3, 450, 200, 45, 25, 45)
	,('Hospital �1', 3, 3, 500, 250, 50, 30, 50)
	,('Polyclinic �11', 4, 2, 10, 70, 7, 7, 14)
	,('Polyclinic �20', 5, 2, 0, 80, 8, 8, 16)
	,('Polyclinic �40', 6, 2, 10, 90, 9, 9, 18);

INSERT INTO medical_sch.patient (name, surname, date_of_birth, sex, contacts_id)
VALUES
	 ('Bell','Reed','11-01-20', 'W', 7)
	,('Mikayla','Clements','21-01-20', 'W', 8)
	,('Naomi','Delacruz','02-01-21', 'W', 9)
	,('Colby','Mcintosh','25-09-20', 'W', 10)
	,('Salvador','Shields','01-12-20', 'M', 11)
	,('Jamal','Mckay','30-03-20', 'M', 12)
	,('Brent','Johnson','12-08-20', 'M', 13)
	,('Desiree','Riggs','29-12-19', 'W', 14)
	,('Minerva','Moore','19-04-21', 'W', 15)
	,('Steven','Zamora','08-08-20', 'M', 16)
	,('Ulric','Barker','01-01-20', 'M', 17)
	,('Wyatt','Osborne','07-08-20', 'M', 18)
	,('Graham','Vargas','14-09-20', 'M', 19)
	,('Celeste','Blake','19-08-21', 'W', 20);

INSERT INTO medical_sch.address (address, medical_institution_id, patient_id)
VALUES
	 ('Ap #803-3962 Donec Avenue', 1, NULL)
	,('9258 Neque Rd.', 2, NULL)
	,('2808 Nec, Ave', 3, NULL)
	,('Ap #867-6871 Aliquet, St.', 4, NULL)
	,('Ap #737-985 Posuere, Ave', 5, NULL)
	,('151-8768 Risus. Rd.', 6, NULL)
	,('4569 Nisl Rd.', NULL, 1)
	,('P.O. Box 898, 5467 Mollis Street', NULL, 2)
	,('849-1243 Nullam Street', NULL, 3)
	,('Ap #564-7847 Amet St.', NULL, 4)
	,('Ap #112-8663 Turpis Avenue', NULL, 5)
	,('Ap #538-865 Felis. Ave', NULL, 6)
	,('P.O. Box 704, 1417 Gravida Rd.', NULL, 7)
	,('680-1840 Nunc Road', NULL, 8)
	,('4058 Dis Rd.', NULL, 9)
	,('P.O. Box 549, 6326 Curae; Avenue', NULL, 10)
	,('P.O. Box 675, 9075 Eu Avenue', NULL, 11)
	,('145-5797 Ornare Avenue', NULL, 12)
	,('P.O. Box 576, 5486 Orci, Rd.', NULL, 13)
	,('P.O. Box 242, 4755 Lorem, St.', NULL, 14);

INSERT INTO medical_sch.location (name, address_id, location_type_id)
VALUES
	 ('103', 1, 6)
	,('233', 5, 7)
	,('M1', 1, 6)
	,('M12', 5, 7)
	,('56', 1, 6)
	,('78', 5, 7)
	,('L2', 1, 6)
	,('L56', 5, 7)
	,('H4', 1, 6)
	,('K3', 5, 7)
	,('K45', 1, 6)
	,('O76', 5, 7)
	,('65', 1, 6)
	,('V2', 5, 7);

INSERT INTO medical_sch.position (name, position_type_id)
VALUES
	 ('doctor', 1)
	,('paramedic', 1)
	,('nurse', 1)
	,('chief medical officer', 1)
	,('hospital department manager', 1)
	,('administrator', 2)
	,('manager', 2)
	,('security guard', 2);


INSERT INTO medical_sch.staff (name, surname, identity_document_number, medical_institution_id, position_id)
VALUES 
	 ('Kim','Blankenship','16671208-1816', 1, 1)
	,('Wanda','Higgins','16150410-5303', 5, 1)
	,('Evan','Serrano','16620814-5471', 1, 1)
	,('Nigel','Richmond','16581208-9257', 1, 1)
	,('Eden','Patrick','16090716-3968', 5, 1)
	,('Christine','Gay','16231220-5731', 5, 1)
	,('Aidan','Conner','16080903-5942', 5, 1)
	,('Oleg','Craft','16961014-7655', 1, 1)
	,('Bert','Harrison','16370729-3514', 1, 1)
	,('Amery','Barber','16731208-7682', 5, 1)
	,('Quon','Sloan','16120429-5883', 1, 1)
	,('Aline','Hodges','16230629-5649', 5, 1)
	,('Cheyenne','Grimes','16420317-3978', 1, 1)
	,('Sigourney','Byers','16360804-6722', 5, 1);

INSERT INTO medical_sch.patient_visit (location_id, patient_id, staff_id, start_date_time, end_date_time)
VALUES
	 (1, 1, 1, '20-02-20 09:00:00', '20-02-20 10:00:00')
	,(2, 2, 5, '07-06-19 09:00:00', '07-06-19 10:00:00')
	,(13, 3, 1, '16-02-20 09:00:00', '16-02-20 10:00:00')
	,(4, 4, 5, '22-04-19 09:00:00', '22-04-19 10:00:00')
	,(2, 5, 5, '17-03-19 09:00:00', '17-03-19 10:00:00')
	,(1, 6, 1, '05-03-19 09:00:00', '05-03-19 10:00:00')
	,(5, 7, 1, '02-11-19 09:00:00', '02-11-19 10:00:00')
	,(7, 8, 1, '12-02-19 09:00:00', '12-02-19 10:00:00')
	,(8, 9, 5, '25-01-19 09:00:00', '25-01-19 10:00:00')
	,(3, 10, 1, '19-08-19 09:00:00', '19-08-19 10:00:00')
	,(1, 11, 1, '30-12-19 09:00:00', '30-12-19 10:00:00')
	,(5, 12, 1, '19-11-19 09:00:00', '19-11-19 10:00:00')
	,(4, 13, 5, '22-12-19 09:00:00', '22-12-19 10:00:00')
	,(2, 14, 5, '18-06-19 09:00:00', '18-06-19 10:00:00');	

COMMIT

-- Part 2. Write a query to identify doctors with insufficient workload (less than 5 patients a month for the past few months)

SELECT 
	  DISTINCT (CONCAT (s2."name", ' ', s2.surname)) AS doctor_name
FROM (	
	  SELECT 
	  	pv.staff_id
	  , COUNT (DISTINCT pv.patient_id) AS patients_number
	  , EXTRACT (MONTH FROM pv.end_date_time) AS m
	  , EXTRACT (YEAR FROM pv.end_date_time) AS y
	  FROM patient_visit pv
      JOIN staff s 
	  	ON pv.staff_id = s.staff_id 
	  JOIN "position" p 
		ON s.position_id = p.position_id 
		AND p."name" = 'doctor'
	  WHERE pv.end_date_time BETWEEN '2019-01-01 00:00:00' AND '2020-03-31 23:59:59'  							-- last few months
	  GROUP BY pv.staff_id, EXTRACT (YEAR FROM pv.end_date_time), EXTRACT (MONTH FROM pv.end_date_time)
	  HAVING COUNT (DISTINCT pv.patient_id) < 5
	) AS rs
JOIN staff s2
	ON rs.staff_id = s2.staff_id;	

	

	