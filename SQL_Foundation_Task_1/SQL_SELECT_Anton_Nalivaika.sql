-- 1.1 All comedy movies released between 2000 and 2004, alphabetical

SELECT 
	  f.title
	, c.name AS film_genre
	, f.release_year 
FROM film_category fc 
JOIN category c
	ON  fc.category_id=c.category_id 
	AND c.name ='Comedy'
JOIN film f 
	ON  fc.film_id=f.film_id 
	AND f.release_year >= '2000' 
	AND f.release_year <='2004'
ORDER BY f.title; 


-- 1.2 Revenue of every rental store for year 2017 (columns: address and address2 � as one column, revenue)

SELECT 
	  CONCAT (a.address, a.address2) AS store_location
	, SUM(p.amount) AS revenue
FROM store s 
JOIN address a
	ON s.address_id=a.address_id 
JOIN staff stf 
	ON s.store_id = stf.store_id 
JOIN payment p 
	ON stf.staff_id = p.staff_id 
WHERE p.payment_date BETWEEN '2017-01-01 00:00:00' AND '2017-12-31 23:59:59' 
GROUP BY stf.store_id, a.address_id;


-- 1.3 Top-3 actors by number of movies they took part in (columns: first_name, last_name, number_of_movies, sorted by number_of_movies in descending order)

SELECT 
	  a.first_name
	, a.last_name  
	, COUNT(*) AS number_of_movies 
FROM film_actor fa
JOIN actor a 
	ON fa.actor_id = a.actor_id 
GROUP BY fa.actor_id, a.actor_id 
ORDER BY number_of_movies DESC
LIMIT 3; 


/* 1.4 Number of comedy, horror and action movies per year (columns: release_year, number_of_action_movies, number_of_horror_movies, number_of_comedy_movies), 
   sorted by release year in descending order */ 

SELECT 
	  f.release_year
	, SUM (CASE WHEN c.name='Action' THEN 1 ELSE 0 END) AS number_of_action_movies  
	, SUM (CASE WHEN c.name='Horror' THEN 1 ELSE 0 END) AS number_of_horror_movies
	, SUM (CASE WHEN c.name='Comedy' THEN 1 ELSE 0 END) AS number_of_comedy_movies
FROM film f
JOIN film_category fc 
	ON f.film_id = fc.film_id 
JOIN category c
	ON fc.category_id = c.category_id 
WHERE c.name IN ('Action','Horror','Comedy')
GROUP BY f.release_year 
ORDER BY f.release_year DESC 


-- 1.5 Which staff members made the highest revenue for each store and deserve a bonus for 2017 year?

SELECT 
	  DISTINCT ON (s.store_id) store_id
	, sa.sum_amount
	, CONCAT (s.first_name, ' ', s.last_name)
FROM ( 
		SELECT 
			  SUM (p.amount) AS sum_amount
			, p.staff_id
		FROM payment p	
		WHERE p.payment_date BETWEEN '2017-01-01 00:00:00' AND '2017-12-31 23:59:59'
		GROUP BY p.staff_id
	) AS sa
JOIN staff s 
	ON sa.staff_id = s.staff_id
ORDER BY s.store_id, sa.sum_amount DESC; 


-- 1.6 Which 5 movies were rented more than others and what's expected audience age for those movies?

SELECT
	  mr.how_much_rented
	, mr.film_id
	, f.title AS film_title
	, f.rating AS audience_age  
	, CASE
		WHEN f.rating = 'PG' THEN 'Parental Guidance Suggested � Some material may not be suitable for children'
		WHEN f.rating = 'PG-13' THEN 'Parents Strongly Cautioned � Some material may be inappropriate for children under 13'
		WHEN f.rating = 'NC-17' THEN 'No children under 17 admitted'
	  END audience_age_description			
FROM (
		SELECT 
			  COUNT (*) AS how_much_rented 
			, i.film_id
		FROM rental r 
		JOIN inventory i 
			ON r.inventory_id = i.inventory_id
		GROUP BY i.film_id 	
		ORDER BY how_much_rented DESC, i.film_id
		LIMIT 5
	) AS mr
JOIN film f
	ON mr.film_id = f.film_id; 


-- 1.7 Which actors/actresses didn't act for a longer period of time than others?


CREATE SEQUENCE IF NOT EXISTS temp_seq;
WITH b AS (
	WITH actor_career AS (
		SELECT 
			  NEXTVAL('temp_seq') AS new_number
			, a.actor_id  
			, f.release_year 
		FROM film f
		JOIN film_actor fa 
			ON f.film_id = fa.film_id 
		JOIN actor a 
			ON fa.actor_id = a.actor_id 
		ORDER BY a.actor_id, f.release_year DESC 	
	)
	SELECT  
		  CONCAT (a2.first_name, ' ', a2.last_name) AS actor	
		, COALESCE (ac2.release_year, EXTRACT(YEAR FROM CURRENT_DATE)) - ac1.release_year AS career_break
	FROM actor_career ac1
	LEFT JOIN actor_career ac2
		ON ac1.new_number = ac2.new_number+1
		AND ac1.actor_id = ac2.actor_id
	JOIN actor a2 
		ON ac1.actor_id = a2.actor_id 		
)
SELECT DISTINCT actor
FROM b
WHERE career_break = 
	( 
		SELECT career_break 
		FROM b 
		ORDER BY career_break DESC 
		LIMIT 1);
	