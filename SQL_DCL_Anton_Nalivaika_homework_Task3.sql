/* 3. Read about row-level security (https://www.postgresql.org/docs/12/ddl-rowsecurity.html) and configure it for your database, so that the
customer can only access his own data in "rental" and "payment" tables (verify using the personalized role you previously created).*/

--RESET ROLE;
--SELECT SESSION_USER, CURRENT_USER;


GRANT SELECT ON rental TO customer;
GRANT SELECT ON payment TO customer;

ALTER TABLE rental ENABLE ROW LEVEL SECURITY;
ALTER TABLE payment ENABLE ROW LEVEL SECURITY;


CREATE POLICY ROBIN_HAYES_rental ON rental TO client_ROBIN_HAYES
    USING (rental.customer_id = 100);
CREATE POLICY ROBIN_HAYES_payment ON payment TO client_ROBIN_HAYES
    USING (payment.customer_id = 100);


   
--SET ROLE client_ROBIN_HAYES;
--SELECT * FROM rental;
--SELECT * FROM payment;
