/* Task 1. Build the query to generate a report about the most significant customers (which have maximum sales) through various sales channels.
		   The 5 largest customers are required for each channel.
		   Column sales_percentage shows percentage of customerís sales within channel sales. */ 

SELECT																			--	Final result with TOP 5 customer 
	  r2.channel_desc
	, r2.cust_last_name
	, r2.cust_first_name
	, r2.amount_sold
	, r2.sales_percentage
FROM (
		SELECT																	-- Preliminary result set
			  c.channel_desc
			, cst.cust_last_name
			, cst.cust_first_name
			, r1.amount_sold
			, ROW_NUMBER () OVER (PARTITION BY c.channel_desc ORDER BY r1.amount_sold DESC) AS row_num
			, CONCAT(
					 ROUND(
					       (r1.amount_sold * 100) / (SUM (r1.amount_sold) OVER (PARTITION BY c.channel_desc)), 5), ' %') AS sales_percentage
		FROM (
				SELECT 
			  		  s.cust_id 
					, s.channel_id
					, SUM (s.amount_sold) AS amount_sold 
				FROM sales s
				GROUP BY s.cust_id, s.channel_id 
			 ) AS r1
		JOIN channels c 
			ON r1.channel_id = c.channel_id 
		JOIN customers cst
			ON r1.cust_id = cst.cust_id
	 ) AS r2	
WHERE r2.row_num < 6 



/* Task 2. Compose query to retrieve data for report with sales totals for all products in Photo category in Asia (use data for 2000 year). 
           Calculate report total (YEAR_SUM). */

--CREATE EXTENSION tablefunc

SELECT 
	  frez.product_name
	, frez.q1
	, frez.q2
	, frez.q3
	, frez.q4
	, COALESCE (frez.q1, 0) + COALESCE (frez.q2, 0) + COALESCE (frez.q3, 0) + COALESCE (frez.q4, 0) AS year_sum 
FROM (
		SELECT * 
		FROM crosstab (
				'SELECT 	
					  r2.prod_name 
					, r2.quarters 
					, r2.sum   
				 FROM (
						
						SELECT
							  r1.prod_name
							, r1.quarters 
							, SUM (r1.amount_sold) 
						FROM (
								SELECT 
									  p.prod_name   
									, CASE 
											WHEN EXTRACT(QUARTER FROM s.time_id) = 1 THEN ''q1'' 
											WHEN EXTRACT(QUARTER FROM s.time_id) = 2 THEN ''q2''
											WHEN EXTRACT(QUARTER FROM s.time_id) = 3 THEN ''q3''
											WHEN EXTRACT(QUARTER FROM s.time_id) = 4 THEN ''q4''
									  END AS quarters
									, s.amount_sold
								FROM sales s 
								JOIN products p 
									ON s.prod_id = p.prod_id AND p.prod_category = ''Photo''
								JOIN customers c
									ON s.cust_id = c.cust_id
								JOIN countries c2 
									ON c.country_id = c2.country_id 
									AND c2.country_region = ''Asia''
								WHERE s.time_id BETWEEN ''2000-01-01'' AND ''2000-12-31''
							 ) AS r1
						GROUP BY r1.prod_name, r1.quarters 		
		
					  ) AS r2
				 WHERE r2.quarters IN (''q1'', ''q2'', ''q3'', ''q4'')')
		AS so (product_name varchar(50), q1 numeric(10,2), q2 numeric(10,2), q3 numeric(10,2), q4 numeric(10,2))
) AS frez

	
/* Task 3. Build the query to generate a report about customers who were included into TOP 300 (based on the amount of sales) in 1998, 1999 and 2001. 
 		   This report should separate clients by sales channels, and, at the same time, channels should be calculated independently 
 		   (i.e. only purchases made on selected channel are relevant). */

SELECT 																				-- Final result with TOP 300 best clients
	  c.channel_desc
	, r1.cust_id
	, cst.cust_last_name 
	, cst.cust_first_name
	, r1.amount_sold
FROM (
		SELECT 																		-- Preliminary result with data for (1998, 1999, 2001) and calculated amound_sold (by channel_id, cust_id) 
			  s.channel_id 
			, s.cust_id
			, SUM (s.amount_sold) AS amount_sold
			, ROW_NUMBER () OVER (ORDER BY SUM (s.amount_sold) DESC) AS row_num		
		FROM sales s 
		WHERE EXTRACT (YEAR FROM s.time_id)	IN (1998, 1999, 2001)
		GROUP BY s.channel_id, s.cust_id  
		ORDER BY amount_sold DESC
	 ) AS r1
JOIN channels c 
	ON r1.channel_id = c.channel_id 
JOIN customers cst 
	ON r1.cust_id = cst.cust_id 
WHERE r1.row_num < 301	



/* Task 4. Build the query to generate the report about sales in America and Europe: */

SELECT 																										--1 variant - without window functions
	  r1.calendar_month_desc
	, r1.prod_category
	, SUM (CASE WHEN r1.country_region = 'Americas' THEN r1.sum_amount_sold ELSE 0 END) AS americas_SALES
	, SUM (CASE WHEN r1.country_region = 'Europe' THEN r1.sum_amount_sold ELSE 0 END) AS europe_SALES
FROM (
		SELECT
			  t.calendar_month_desc 
			, p.prod_category  
			, c2.country_region
			, SUM (s.amount_sold) AS sum_amount_sold
		FROM sales s 
		JOIN customers c 
			ON s.cust_id = c.cust_id 
		JOIN countries c2 
			ON c.country_id = c2.country_id
			AND c2.country_region IN ('Europe', 'Americas')
		JOIN times t 
			ON s.time_id = t.time_id 
			AND t.calendar_month_desc IN ('2000-01', '2000-02', '2000-03')
		JOIN products p 
			ON s.prod_id = p.prod_id 
		GROUP BY t.calendar_month_desc, p.prod_category, c2.country_region
	 ) r1
GROUP BY r1.calendar_month_desc, r1.prod_category

	 
SELECT 																										--2 variant - with window functions
	  r1.calendar_month_desc 
	, r1.prod_category
	, SUM (r1.americas_SALES) AS americas_SALES
	, SUM (r1.europe_SALES) AS europe_SALES
FROM (
          SELECT DISTINCT
			  t.calendar_month_desc 
			, p.prod_category  
			, c2.country_region
			, SUM (s.amount_sold) FILTER (WHERE c2.country_region = 'Americas') OVER (PARTITION BY t.calendar_month_desc, p.prod_category, c2.country_region) AS americas_SALES
			, SUM (s.amount_sold) FILTER (WHERE c2.country_region = 'Europe') OVER (PARTITION BY t.calendar_month_desc, p.prod_category, c2.country_region) AS europe_SALES
		FROM sales s
		JOIN customers c 
			ON s.cust_id = c.cust_id 
		JOIN countries c2 
			ON c.country_id = c2.country_id
			AND c2.country_region IN ('Europe', 'Americas')
		JOIN times t 
			ON s.time_id = t.time_id 
			AND t.calendar_month_desc IN ('2000-01', '2000-02', '2000-03')
		JOIN products p 
			ON s.prod_id = p.prod_id 
		ORDER BY 1,2,3
	 ) r1
GROUP BY r1.calendar_month_desc, r1.prod_category



