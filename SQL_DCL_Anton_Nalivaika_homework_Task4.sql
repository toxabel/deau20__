 /* 4.  Prepare answers to the following questions:
	4.1 How can one restrict access to certain columns of a database table?*/
	
-- RESET ROLE;
-- SELECT SESSION_USER, CURRENT_USER;

 
REVOKE SELECT ON actor FROM customer;
GRANT SELECT (actor_id, first_name, last_name) ON actor TO customer;


-- SET ROLE customer;
-- SELECT actor_id, first_name, last_name FROM actor;



 --	4.2 What is the difference between user identification and user authentication?
    

/* Identification is the ability to identify uniquely a user of a system 
   (check user login). 
   Authentication is the ability to prove that user is genuinely who that person claims to be 
   (password chack for that login). */ 
    


 -- 4.3 What are the recommended authorization protocols for PostgreSQL?



/* If DB servers are isolated from the world should use the SCRAM-SHA-256 authentication method and limit the IP addresses.
   If the application server�business logic�and database server are not on the same machine, should use a strong authentication method, such as LDAP and Kerberos. */



 -- 4.4 What is proxy authentication in PostgreSQL and what is it for? Why does it make the previously discussed role-based access control easier to implement?


	/* Application for connection to the database use 'connection_login1', after that invokes the SET ROLE command based on the user class. 
	   It is another level of security that allowing an authorized user to perform a certain task. This is proxy authentication.
	   
	   It simplifies the task to implement role-based access control because the database role system can also be used to partially implement
	   this logic by delegating the authentication to another role after the connection is established or reused,
	   using the SET SESSION AUTHORIZATION statement or SET ROLE command in a transaction block.*/
	   
