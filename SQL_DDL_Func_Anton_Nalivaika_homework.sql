/* 1. Create function that inserts new movie with the given name in �film� table. �release_year�, �language� are optional arguments and default to
	  current year and Russian respectively. The function must return film_id of the inserted movie. */

CREATE OR REPLACE FUNCTION add_new_film 
(
     title TEXT
   , release_year YEAR DEFAULT EXTRACT (YEAR FROM now()) 
)
RETURNS INT4 AS
$$	

DECLARE 
	language_id_rus INT4 ;
    film_id_out INT4;  
BEGIN  
	SELECT lng.language_id INTO language_id_rus
	FROM "language" lng
	WHERE lng."name" = 'Russian';	
	  
IF language_id_rus IS NULL 
THEN
	INSERT INTO "language"(name) 
	VALUES 
			('Russian')
	RETURNING "language".language_id INTO language_id_rus;
END IF; 		
	
INSERT INTO film (title, release_year, language_id)
VALUES
	($1, $2, language_id_rus )
RETURNING film.film_id INTO film_id_out;
RETURN film_id_out;

END
$$ 
LANGUAGE plpgsql;


SELECT add_new_film ('MULAN'::text, 2020::year);



/* 2.1 What operations do the following functions perform: film_in_stock, film_not_in_stock, inventory_in_stock, get_customer_balance,
	   inventory_held_by_customer, rewards_report, last_day? You can find these functions in dvd_rental database */

-- film_in_stock	
							/* select inventory_id of interested movie from a certain store and exec function inventory_in_stock with paramet inventory_id,
							 * return all available (copies in-store) inventory_id of interested movie from a certain store*/

-- film_not_in_stock		
							/* select inventory_id of interested movie from a certain store and exec function inventory_in_stock with paramet inventory_id,  
							 * return all not available (copies not in-store) inventory_id of interested movie from a certain store */		

-- inventory_in_stock		
							/* The first part of the function checks how many times a copy of the film (inventory id) has been rented if not once, returns TRUE. 
							 * Else the second part checks and returns TRUE if all clients returned a copy of the film (inventory id) to the store or FALSE. */

-- get_customer_balance		
							/* Function get_customer_balance returns amount that the client spent on the service for renting DVD disks 
							 * (FEES PAID TO RENT + FEES FOR PRIOR RENTALS + PAYMENTS MADE PREVIOUSLY) */


-- inventory_held_by_customer	
							/* Function inventory_held_by_customer return customer_id of client who did not return current copy of film (inventory id). */
			

-- rewards_report			
							/* Function rewards_report creates parameters and check input data, exec INSERT INTO script and insert the result
							 * (with all customers meeting the monthly purchase requirements) in a temporary table. Join information about
							 * our customers and output result using the cycle. Then function clean up the data in a temporary table.*/


-- last_day					
							/* Function last_day using the conditional expression CASE, get the current month from a given date and compare it with 12 (December).
							 * If true, add one year to the current date and change the month and day to -01-01, then subtract 1 day. 
							 * Else extract the year and month from the current date add one month and change the day to -01, then subtract 1 day. */



-- 2.2 Why does �rewards_report� function return 0 rows? Correct and recreate the function, so that it's able to return rows properly.

		-- The rewards_report function return 0 rows because there is no data in DB in this time interval (CURRENT_DATE - '3 month').
		
CREATE OR REPLACE FUNCTION npublic.rewards_report(min_monthly_purchases integer, min_dollar_amount_purchased numeric)
	RETURNS SETOF npublic.customer
 	LANGUAGE plpgsql
 	SECURITY DEFINER
	AS $function$
DECLARE
    last_month_start DATE;
    last_month_end DATE;
	rr RECORD;
	tmpSQL TEXT;
BEGIN
    /* Some sanity checks... */
    IF min_monthly_purchases = 0 THEN
        RAISE EXCEPTION 'Minimum monthly purchases parameter must be > 0';
    END IF;
    IF min_dollar_amount_purchased = 0.00 THEN
        RAISE EXCEPTION 'Minimum monthly dollar amount purchased parameter must be > $0.00';
    END IF;

    last_month_start := CURRENT_DATE - '45 month'::interval;
    last_month_start := to_date((extract(YEAR FROM last_month_start) || '-' || extract(MONTH FROM last_month_start) || '-01'),'YYYY-MM-DD');
    last_month_end := LAST_DAY(last_month_start);
   
    /*
    Create a temporary storage area for Customer IDs.
    */
   
    CREATE TEMPORARY TABLE tmpCustomer (customer_id INTEGER NOT NULL PRIMARY KEY);
   
    /*
    Find all customers meeting the monthly purchase requirements
    */
   
    tmpSQL := 'INSERT INTO tmpCustomer (customer_id)
        SELECT p.customer_id
        FROM payment AS p
        WHERE DATE(p.payment_date) BETWEEN '||quote_literal(last_month_start) ||' AND '|| quote_literal(last_month_end) || '
        GROUP BY customer_id
        HAVING SUM(p.amount) > '|| min_dollar_amount_purchased || '
        AND COUNT(customer_id) > ' ||min_monthly_purchases ;

    EXECUTE tmpSQL;
   
    /*
    Output ALL customer information of matching rewardees.
    Customize output as needed.
    */
    FOR rr IN EXECUTE 'SELECT c.* FROM tmpCustomer AS t INNER JOIN customer AS c ON t.customer_id = c.customer_id' LOOP
        RETURN NEXT rr;
    END LOOP;
   
    /* Clean up */
    tmpSQL := 'DROP TABLE tmpCustomer';
    EXECUTE tmpSQL;

RETURN;
END
$function$
;

SELECT npublic.rewards_report (1, 1)



-- 2.3 Is there any function that can potentially be removed from the dvd_rental codebase? If so, which one and why?

		-- Function film_not_in_stock can potentially be removed because this function does the same as the function film_in_stock except for condition NOT.
		   


/* 2.4 The �get_customer_balance� function describes the business requirements for calculating the client balance. Unfortunately, not all of
	   them are implemented in this function. Try to change function using the requirements from the comments.*/

CREATE OR REPLACE FUNCTION npublic.get_customer_balance(p_customer_id integer, p_effective_date timestamp with time zone)
 RETURNS numeric
 LANGUAGE plpgsql
AS $function$
       --#OK, WE NEED TO CALCULATE THE CURRENT BALANCE GIVEN A CUSTOMER_ID AND A DATE
       --#THAT WE WANT THE BALANCE TO BE EFFECTIVE FOR. THE BALANCE IS:
       --#   1) RENTAL FEES FOR ALL PREVIOUS RENTALS
       --#   2) ONE DOLLAR FOR EVERY DAY THE PREVIOUS RENTALS ARE OVERDUE
       --#   3) IF A FILM IS MORE THAN RENTAL_DURATION * 2 OVERDUE, CHARGE THE REPLACEMENT_COST
       --#   4) SUBTRACT ALL PAYMENTS MADE BEFORE THE DATE SPECIFIED
DECLARE
    v_rentfees DECIMAL(5,2); --#FEES PAID TO RENT THE VIDEOS INITIALLY
    v_overfees INTEGER;      --#LATE FEES FOR PRIOR RENTALS
    v_payments DECIMAL(5,2); --#SUM OF PAYMENTS MADE PREVIOUSLY
BEGIN
    SELECT COALESCE(SUM(film.rental_rate),0) INTO v_rentfees
    FROM film, inventory, rental
    WHERE film.film_id = inventory.film_id
      AND inventory.inventory_id = rental.inventory_id
      AND rental.rental_date <= p_effective_date
      AND rental.customer_id = p_customer_id;

    SELECT COALESCE(SUM(CASE 
                           WHEN (rental.return_date - rental.rental_date) > (film.rental_duration * '1 day'::interval)
                           THEN EXTRACT(epoch FROM ((rental.return_date - rental.rental_date) - (film.rental_duration * '1 day'::interval)))::INTEGER / 86400 -- * 1 dollar
                           ELSE 0
                        END),0) 
    INTO v_overfees
    FROM rental, inventory, film
    WHERE film.film_id = inventory.film_id
      AND inventory.inventory_id = rental.inventory_id
      AND rental.rental_date <= p_effective_date
      AND rental.customer_id = p_customer_id;

    SELECT COALESCE(SUM(payment.amount),0) INTO v_payments
    FROM payment
    WHERE payment.payment_date <= p_effective_date
    AND payment.customer_id = p_customer_id;

    RETURN v_rentfees + v_overfees - v_payments;
END
$function$
;

-- 2.5 How do �group_concat� and �_group_concat� functions work? (database creation script might help) Where are they used?
		
		-- The functions is found in views, concatenates values from a column into one row in grouped data.


-- 2.6 What does �last_updated� function do? Where is it used? 
	
		/* Each table has trigger 'last_updated' which executes function 'last_updated' for each row before updating data.   
		   Function last_updated change date in last_update field by CURRENT_TIMESTAMP */


-- 2.7 What is tmpSQL variable for in �rewards_report� function? Can this function be recreated without EXECUTE statement and dynamic SQL? Why?

		-- Variable tmpSQL stores the INSERT INTO ... script as a text, which after exec insert result set into a temporary table.
		-- Yes it is possible to recreate function without EXECUTE statement and dynamic SQL. Script below.
		
		-- DROP FUNCTION npublic.rewards_report
		CREATE OR REPLACE FUNCTION npublic.rewards_report
		(
			  min_monthly_purchases integer
			, min_dollar_amount_purchased numeric
		)
		RETURNS TABLE 
		(
			  customer_id_out INT4
			, store_id_out INT2
			, first_name_out TEXT
			, last_name_out TEXT
			, email_out TEXT
			, address_id_out INT2
			, activebool_out BOOL
			, create_date_out DATE
			, last_update_out TIMESTAMPTZ
			, active_out INT4
		) AS 
		$$
		
		DECLARE
		    last_month_start DATE;
		    last_month_end DATE;
		BEGIN
		    /* Some sanity checks... */
		    IF min_monthly_purchases = 0 THEN
		        RAISE EXCEPTION 'Minimum monthly purchases parameter must be > 0';
		    END IF;
		    IF min_dollar_amount_purchased = 0.00 THEN
		        RAISE EXCEPTION 'Minimum monthly dollar amount purchased parameter must be > $0.00';
		    END IF;
		
		    last_month_start := CURRENT_DATE - '45 month'::interval;
		    last_month_start := to_date((extract(YEAR FROM last_month_start) || '-' || extract(MONTH FROM last_month_start) || '-01'),'YYYY-MM-DD');
		    last_month_end := LAST_DAY(last_month_start);
		    
		RETURN QUERY
			SELECT 
				  c.customer_id
				, c.store_id 
				, c.first_name 
				, c.last_name 
				, c.email 
				, c.address_id 
				, c.activebool 
				, c.create_date 
				, c.last_update 
				, c.active 
			FROM 
			(
				SELECT p.customer_id 
			    FROM payment AS p
			    WHERE DATE(p.payment_date) BETWEEN last_month_start AND last_month_end
			    GROUP BY customer_id
			    HAVING SUM(p.amount) > min_dollar_amount_purchased
			    AND COUNT(customer_id) > min_monthly_purchases
			) AS t
			JOIN customer AS c
				ON t.customer_id = c.customer_id;
		   
		END;
		$$
		LANGUAGE plpgsql;
		
		SELECT * FROM npublic.rewards_report (1, 1)
