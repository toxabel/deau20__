/* 1. Analyze annual sales by channels and regions. Build the query to generate the same report:
	  Conditions:
	  			country_region: 'Americas', 'Asia', 'Europe'
				calendar_year: 1999, 2000, 2001
				ordering: country_region ASC, calendar_year ASC, channel_desc ASC
	  Columns description:
     			AMOUNT_SOLD � amount sales for channel
				% BY CHANNELS � percentage of sales for the channel (e.g. 100% - total sales for Americas in 1999, 63.64% - percentage of sales for the channel �Direct Sales�)
				% PREVIOUS PERIOD � the same value as in the % BY CHANNELS column, but for the previous year
				% DIFF � difference between % BY CHANNELS and % PREVIOUS PERIOD */


SELECT																											-- final result set
	  r2.country_region 
	, r2.calendar_year
	, r2.channel_desc
	, CONCAT (TO_CHAR (r2.amount_sold, '9,999,999,999'), ' $') AS amount_sold 
	, CONCAT (r2.by_channels, ' %') AS "%_by_channels"
	, CONCAT (LAG (r2.by_channels, 1) OVER w2, ' %') AS "%_previous_period"										-- percentage of sales for the channel for previous year (with out 1998)
	, CONCAT (r2.by_channels - (LAG (r2.by_channels, 1) OVER w2), ' %') AS "%_diff"
FROM (
		SELECT 																									-- previous result set plus new column with percentage of sales for the channel
			  r1.* 
			, ROUND (r1.amount_sold * 100 / SUM (r1.amount_sold) OVER w1, 2) AS by_channels
		FROM (
				SELECT 																							-- result set with the values of the first 4 columns (for 1999, 2000, 2001)
					  c2.country_region 
					, EXTRACT (YEAR FROM s.time_id) AS calendar_year
					, c3.channel_desc
					, SUM (s.amount_sold) AS amount_sold
				FROM sales s 
				JOIN customers c 
					ON s.cust_id = c.cust_id 
				JOIN countries c2 
					ON c.country_id = c2.country_id 
					AND c2.country_region IN ('Americas', 'Asia', 'Europe')
				JOIN channels c3 
					ON s.channel_id = c3.channel_id
				WHERE EXTRACT (YEAR FROM s.time_id) IN (1999, 2000, 2001)
				GROUP BY c2.country_region, EXTRACT (YEAR FROM s.time_id), c3.channel_desc
				ORDER BY 1, 2, 3
			 ) r1	
		WINDOW w1 AS (PARTITION BY r1.country_region, r1.calendar_year ORDER BY r1.country_region)	 			-- allocating partition for counting percentage of sales for the channel
	 ) r2
WINDOW w2 AS (PARTITION BY r2.country_region, r2.channel_desc ORDER BY r2.calendar_year)						-- allocating partition to get the percentage of sales for the channel for previous year
ORDER BY 1, 2, 3



/* 2. Build the query to generate a sales report for the 49th, 50th and 51st weeks of 1999. Add column CUM_SUM for accumulated amounts within weeks. 
	  For each day, display the average sales for the previous, current and next days (centered moving average, CENTERED_3_DAY_AVG column).
	  For Monday, calculate average weekend sales + Monday + Tuesday. For Friday, calculate the average sales for Thursday + Friday + weekends. */


SELECT 																											-- final result set
	  r1.calendar_week_number
	, r1.time_id
	, r1.day_name
	, r1.sales
	, SUM (r1.sales) OVER (PARTITION BY r1.calendar_week_number ORDER BY r1.time_id 
						   RANGE BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS cum_sum						-- allocating partition for counting accumulated amounts within weeks	
	, CASE 
			WHEN r1.day_name = 'Friday'
		 	THEN ROUND (AVG (r1.sales) OVER (ORDER BY r1.time_id
						   RANGE BETWEEN INTERVAL '1' DAY PRECEDING AND INTERVAL '2' DAY FOLLOWING), 2) 		-- counting value for colomn centered_3_day_avg (Friday = Thursday + Friday + weekends)
			
			WHEN r1.day_name = 'Monday'
			THEN ROUND (AVG (r1.sales) OVER (ORDER BY r1.time_id
						   RANGE BETWEEN INTERVAL '2' DAY PRECEDING AND INTERVAL '1' DAY FOLLOWING), 2)			-- counting value for colomn centered_3_day_avg (Monday = weekends + Monday + Tuesday)
			
			ELSE ROUND (AVG (r1.sales) OVER (ORDER BY r1.time_id
						   RANGE BETWEEN INTERVAL '1' DAY PRECEDING AND INTERVAL '1' DAY FOLLOWING), 2)
	  END AS centered_3_day_avg																					-- allocating partition for counting average sales for the previous, current and next days (exept Monday and Friday)
FROM (
		SELECT																									-- result set with the values of the first 4 columns (for 49th, 50th and 51st weeks of 1999)
			  t.calendar_week_number 
			, s.time_id 
			, t.day_name 
			, SUM (s.amount_sold) AS sales
		FROM sales s 
		JOIN times t 
			ON s.time_id = t.time_id 
			AND t.calendar_week_number IN (49, 50, 51)
			AND t.calendar_year = 1999
		GROUP BY t.calendar_week_number, t.day_name, s.time_id	
		ORDER BY s.time_id
	) r1

	
	
/* 3. Prepare 3 examples of using window functions with a frame clause (RANGE, ROWS, and GROUPS modes)
	  Explain why you used a particular type of frame in each example. It can be one query or 3 separate queries. */
	
   -- RENGE vs GOUPS
SELECT 
	  t.calendar_month_number 
	, SUM (s.amount_sold) as sales
	, SUM (SUM (s.amount_sold)) OVER (ORDER BY t.calendar_month_number RANGE BETWEEN 1 PRECEDING AND CURRENT ROW) AS range_test		-- sum value of sales for current month and previous month, if sales were not interrupted (there no sales for a whole month)
	, ARRAY_AGG (SUM (s.amount_sold)) OVER (ORDER BY t.calendar_month_number RANGE BETWEEN 1 PRECEDING AND CURRENT ROW) AS arr_range_test 	
	, SUM (SUM (s.amount_sold)) OVER (ORDER BY t.calendar_month_number GROUPS BETWEEN 1 PRECEDING AND CURRENT ROW) AS groups_test	-- sum value of sales for current period and previous period
	, ARRAY_AGG (SUM (s.amount_sold)) OVER (ORDER BY t.calendar_month_number GROUPS BETWEEN 1 PRECEDING AND CURRENT ROW) AS arr_groups_test	
FROM sales s 
JOIN times t 
	ON s.time_id = t.time_id 
	AND t.calendar_year = 1998
WHERE s.channel_id = 9
GROUP BY t.calendar_month_number 	

	-- ROW vs GOUPS
SELECT 
	  t.calendar_month_number
	, t.calendar_week_number
	, SUM (s.amount_sold) AS sales 
	, SUM (SUM (s.amount_sold)) OVER (ROWS BETWEEN 1 PRECEDING AND CURRENT ROW) AS rows_test		-- sum value of sales for current row and previous row of data set
	, ARRAY_AGG (SUM (s.amount_sold)) OVER (ROWS BETWEEN 1 PRECEDING AND CURRENT ROW) AS arr_rows_test 	
	, SUM (SUM (s.amount_sold)) OVER (ORDER BY t.calendar_month_number GROUPS BETWEEN 1 PRECEDING AND CURRENT ROW) AS groups_test	-- sum value of sales for current period (month) and previous period (month)
	, ARRAY_AGG (SUM (s.amount_sold)) OVER (ORDER BY t.calendar_month_number GROUPS BETWEEN 1 PRECEDING AND CURRENT ROW) AS arr_groups_test	
FROM sales s 
JOIN times t 
	ON s.time_id = t.time_id 
	AND t.calendar_year = 2000
WHERE s.channel_id = 4
GROUP BY t.calendar_month_number, t.calendar_week_number 
ORDER BY t.calendar_month_number, t.calendar_week_number 

	




