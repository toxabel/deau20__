/* 1. Create all tables for the relational model you created while studying DB Basics module (fixed model in 3nf). Create a separate database
	  for them and give it appropriate domain-related name. Make sure you choose optimal datatype for each column. Use NOT NULL
	  constraints, default values and generated columns where appropriate.
   2. Create all table relationships with primary and foreign keys.
   3. Create at least 5 check constraints, not considering unique and not null, on your tables (in total 5, not for each table). */

CREATE DATABASE "kindergarten_ms";

CREATE SCHEMA "kdata";

CREATE TABLE kdata.diet (
  "diet_id" SERIAL PRIMARY KEY,
  "name" TEXT NOT NULL,
  "description" TEXT
);

CREATE TABLE kdata.dish (
  "dish_id" SERIAL PRIMARY KEY,
  "name" TEXT NOT NULL,
  "description" TEXT
);

CREATE TABLE kdata.company (
  "company_id" SERIAL PRIMARY KEY,
  "name" TEXT NOT NULL,
  "email" TEXT CHECK (email LIKE '%@%'),
  "tin" TEXT,
  "phone" TEXT,
  "address" TEXT NOT NULL
);

CREATE TABLE kdata.meals (
  "meals_id" SERIAL PRIMARY KEY,
  "name" TEXT NOT NULL,
  "start_time" TIME NOT NULL,
  "end_time" TIME NOT NULL,
  "description" TEXT
);

CREATE TABLE kdata.group (
  "group_id" SERIAL PRIMARY KEY,
  "name" TEXT NOT NULL,
  "max_group_size" INT2 CHECK (max_group_size > 0) 
);

CREATE TABLE kdata.parent (
  "parent_id" SERIAL PRIMARY KEY,
  "name" TEXT NOT NULL,
  "surname" TEXT NOT NULL,
  "identity_document_number" INT2 NOT NULL
);

CREATE TABLE kdata.tax (
  "tax_id" SERIAL PRIMARY KEY,
  "name" TEXT NOT NULL,
  "tax_rate" INT2 NOT NULL
);

CREATE TABLE kdata.rate (
  "rate_id" SERIAL PRIMARY KEY,
  "name" TEXT NOT NULL,
  "amount" NUMERIC(6,2) NOT NULL CHECK (amount > 0),
  "start_effective_date" DATE NOT NULL DEFAULT now(),
  "end_effective_date" DATE
);

CREATE TABLE kdata.position (
  "position_id" SERIAL PRIMARY KEY,
  "name" TEXT NOT NULL
);

CREATE TABLE kdata.diet_dish (
  "diet_dish_id" SERIAL PRIMARY KEY,
  "diet_id" INT4 NOT NULL REFERENCES kdata.diet,
  "dish_id" INT4 NOT NULL REFERENCES kdata.dish
); 

CREATE TABLE kdata.employee (
  "employee_id" SERIAL PRIMARY KEY,
  "name" TEXT NOT NULL,
  "surname" TEXT NOT NULL,
  "position_id" INT4 NOT NULL REFERENCES kdata.position,
  "identity_document_number" INT2 NOT NULL,
  "group_id" INT4
);

CREATE TABLE kdata.position_salary (
  "position_salary_id" SERIAL PRIMARY KEY,
  "position_id" INT4 NOT NULL REFERENCES kdata.position,
  "salary" NUMERIC(7,2) NOT NULL CHECK (salary > 0), 
  "start_effective_date" DATE NOT NULL DEFAULT now(),
  "end_effective_date" DATE
);

CREATE TABLE kdata.menu (
  "menu_id" SERIAL PRIMARY KEY,
  "date" DATE NOT NULL DEFAULT now(),
  "meals_id" INT4 NOT NULL REFERENCES kdata.meals,
  "diet_id" INT4 NOT NULL REFERENCES kdata.diet,
  "dish_id" INT4 NOT NULL REFERENCES kdata.dish,
  "quantity" INT2
); 

CREATE TABLE kdata.child (
  "child_id" SERIAL PRIMARY KEY,
  "name" TEXT NOT NULL,
  "surname" TEXT NOT NULL,
  "date_of_birth" DATE NOT NULL,
  "sex" TEXT NOT NULL CHECK (sex IN ('M', 'W')),
  "group_id" INT4 NOT NULL REFERENCES kdata.group,
  "diet_id" INT4 NOT NULL REFERENCES kdata.diet
); 

CREATE TABLE kdata.health_status (
  "health_status_id" SERIAL PRIMARY KEY,
  "name" TEXT NOT NULL,
  "child_id" INT4 REFERENCES kdata.child,
  "employee_id" INT4 REFERENCES kdata.employee,
  "description" TEXT,
  "start_effective_date" DATE NOT NULL DEFAULT now(),
  "end_effective_date" DATE
);

CREATE TABLE kdata.visit (
  "visit_id" SERIAL PRIMARY KEY,
  "date" DATE NOT NULL DEFAULT now(),
  "child_id" INT4 NOT NULL REFERENCES kdata.child,
  "arrival_time" TIMESTAMP NOT NULL DEFAULT now(),
  "arrival_parent_id" INT4 NOT NULL REFERENCES kdata.parent,
  "leaving_time" TIMESTAMP CHECK (leaving_time > arrival_time),
  "leaving_parent_id" INT4 REFERENCES kdata.parent
);

CREATE TABLE kdata.service (
  "service_id" SERIAL PRIMARY KEY,
  "name" TEXT NOT NULL,
  "company_id" INT4 NOT NULL REFERENCES kdata.company,
  "description" TEXT
);

CREATE TABLE kdata.relationship (
  "relationsip_id" SERIAL PRIMARY KEY,
  "parent_id" INT4 NOT NULL REFERENCES kdata.parent,
  "child_id" INT4 NOT NULL REFERENCES kdata.child
);

CREATE TABLE kdata.payment_type (
  "payment_type_id" SERIAL PRIMARY KEY,
  "name" TEXT NOT NULL,
  "tax_id" INT4 REFERENCES kdata.tax,
  "position_salary_id" INT4 REFERENCES kdata.position_salary,
  "rate_id" INT4 REFERENCES kdata.rate,
  "service_id" INT4 REFERENCES kdata.service
);

CREATE TABLE kdata.payments (
  "payment_id" SERIAL PRIMARY KEY,
  "date" DATE NOT NULL DEFAULT now(),
  "payment_type_id" INT4 NOT NULL REFERENCES kdata.payment_type,
  "amount" NUMERIC(5,2) NOT NULL
);


-- 4. Fill your tables with sample data (create it yourself, 20+ rows total in all tables, make sure each table has at least 2 rows).

INSERT INTO kdata.diet (name, description)
VALUES 
	  ('Diabetic diet', 'Avoid sugary drinks. Increase daily fiber. Choose healthy carbohydrates at every meal and try to pair them with fat and protein')
	, ('Anti-gluten diet', 'Mainly based on naturally gluten-free foods with a good balance of micro and macronutrients')
	, ('Children''s diet', '')
	, ('Adult diet', '');

INSERT INTO kdata.dish (name, description)
VALUES 
	  ('Jerk Chicken', '')
	, ('Paella', '')
	, ('Borscht', '')
	, ('Salad', '')
	, ('Mashed potatoes','')
	, ('—heesecakes','');
	
INSERT INTO kdata.company (name, email, tin, phone, address)
VALUES 
	  ('—atering for you', 'cat4y@gmail.com',  '12-1234567', '+375293674057', '220126, Belarus, Minsk, Kalesnikova str. 8, 568')
	, ('Cleaning&cleaning', 'clean2@gmail.com', '23-2346578', '+375265699024', '220124, Belarus, Minsk, M.Tanka str. 12, 23');  

INSERT INTO kdata.meals (name, start_time, end_time, description)
VALUES
	  ('Breakfast', '9:00:00', '9:40:00', '')
	, ('Lunch', '13:00:00', '13:50:00', '')
	, ('Afternoon tea', '16:00:00', '16:20:00', '')
	, ('Dinner', '17:30:00', '18:00:00', '');

INSERT INTO kdata.group (name, max_group_size)
VALUES
	  ('First group', 9)
	, ('Second group', 12)
	, ('Third group', 12);  

INSERT INTO kdata.parent (name, surname, identity_document_number)
VALUES
	  ('Kirill', 'Novik', 10005)
	, ('Max', 'Starik', 10006)
	, ('Julia', 'Novik', 10007)
	, ('Elena', 'Starik', 10008);

INSERT INTO kdata.tax (name, tax_rate)
VALUES
	  ('VAT', 20)
	, ('Income tax', 12);  

INSERT INTO kdata.rate (name, amount)
VALUES 
	  ('Half a day', 300)
	, ('Full day', 500);  

INSERT INTO kdata.position (name)
VALUES
	  ('Manager')
	, ('Kindergartener')
	, ('Cook')
	, ('Nurse');  

INSERT INTO kdata.diet_dish (diet_id, dish_id)
VALUES
	  (1, 4)
	, (1, 6)
	, (3, 1)
	, (3, 2);


INSERT INTO kdata.employee (name, surname, position_id, identity_document_number, group_id)
VALUES
	  ('Alex', 'Petrov', 1, 20001, NULL)
	, ('Jon', 'Malkovich', 3, 20002, NULL)
	, ('Anna', 'Bolshova', 2, 20003, 1)
	, ('Alla', 'Zelman', 2, 20004, 2)
	, ('Polina', 'Lorina', 4, 20005, 1)
	, ('Regina', 'Nera', 4, 20006, 2);

INSERT INTO kdata.position_salary (position_id, salary)
VALUES
	  (1, 3000)
	, (2, 2000)
	, (3, 2500)
	, (4, 1500);

INSERT INTO kdata.menu (meals_id, diet_id, dish_id)
VALUES 
	  (1, 3, 4)
	, (1, 3, 6); 

INSERT INTO kdata.child (name, surname, date_of_birth, sex, group_id, diet_id)
VALUES
	  ('Margo', 'Novik', '2018-09-06', 'W', 1, 3)
	, ('Max', 'Novik', '2018-09-06', 'M', 1, 3)
	, ('Julia', 'Starik', '2018-07-25', 'W', 1, 3);

INSERT INTO kdata.health_status (name, child_id, employee_id)
VALUES
	  ('Child health certificate', 1, NULL)
	, ('Child health certificate', 2, NULL)
	, ('Child health certificate', 3, NULL)
	, ('Health certificate', NULL, 1)
	, ('Health certificate', NULL, 2)
	, ('Health certificate', NULL, 3)
	, ('Health certificate', NULL, 4)
	, ('Health certificate', NULL, 5)
	, ('Health certificate', NULL, 6);

INSERT INTO kdata.visit (child_id, arrival_time, arrival_parent_id, leaving_time, leaving_parent_id)
VALUES
	  (1, '2020-09-28 9:00:01', 1, '2020-09-28 17:50:00', 3)
	, (2, '2020-09-28 9:00:01', 1, '2020-09-28 17:50:00', 3)
	, (3, '2020-09-28 9:03:00', 2, '2020-09-28 18:10:00', 4);

INSERT INTO kdata.service (name, company_id)
VALUES
	  ('Food delivery', 1)
	, ('Washing windows', 2)
	, ('Cleaning', 2);  


INSERT INTO kdata.relationship (parent_id, child_id)
VALUES 
	  ( 1, 1)
	, ( 1, 2)
	, ( 3, 1)
	, ( 3, 2)
	, ( 2, 3)
	, ( 4, 3);

INSERT INTO kdata.payment_type (name, tax_id, position_salary_id, rate_id, service_id)
VALUES 
	  ('Tax expenses', 1, NULL, NULL, NULL)
	, ('Tax expenses', 2, NULL, NULL, NULL)
	, ('Salary expenses', NULL, 1, NULL, NULL)
	, ('Salary expenses', NULL, 2, NULL, NULL)
	, ('Salary expenses', NULL, 3, NULL, NULL)
	, ('Salary expenses', NULL, 4, NULL, NULL)
	, ('Income', NULL, NULL, 1, NULL)
	, ('Income', NULL, NULL, 2, NULL)
	, ('Other expenses', NULL, NULL, NULL, 1) 
	, ('Other expenses', NULL, NULL, NULL, 2)
	, ('Other expenses', NULL, NULL, NULL, 3);

INSERT INTO kdata.payments (payment_type_id, amount)
VALUES
	  (8, 500)
	, (8, 500)
	, (8, 500)
	, (11, -100);


/* 5. Alter all tables and add 'record_ts' field to each table. Make it not null and set its default value to current_date. Check that the value
	  has been set for existing rows. */

ALTER TABLE kdata.diet 
ADD COLUMN record_ts DATE NOT NULL DEFAULT now();

ALTER TABLE kdata.dish
ADD COLUMN record_ts DATE NOT NULL DEFAULT now();

ALTER TABLE kdata.company
ADD COLUMN record_ts DATE NOT NULL DEFAULT now();

ALTER TABLE kdata.meals
ADD COLUMN record_ts DATE NOT NULL DEFAULT now();

ALTER TABLE kdata.group
ADD COLUMN record_ts DATE NOT NULL DEFAULT now();
 
ALTER TABLE kdata.parent
ADD COLUMN record_ts DATE NOT NULL DEFAULT now();

ALTER TABLE kdata.tax
ADD COLUMN record_ts DATE NOT NULL DEFAULT now();

ALTER TABLE kdata.rate
ADD COLUMN record_ts DATE NOT NULL DEFAULT now();

ALTER TABLE kdata.position
ADD COLUMN record_ts DATE NOT NULL DEFAULT now();

ALTER TABLE kdata.diet_dish
ADD COLUMN record_ts DATE NOT NULL DEFAULT now();

ALTER TABLE kdata.employee
ADD COLUMN record_ts DATE NOT NULL DEFAULT now();

ALTER TABLE kdata.position_salary
ADD COLUMN record_ts DATE NOT NULL DEFAULT now();

ALTER TABLE kdata.menu
ADD COLUMN record_ts DATE NOT NULL DEFAULT now();

ALTER TABLE kdata.child
ADD COLUMN record_ts DATE NOT NULL DEFAULT now();

ALTER TABLE kdata.health_status
ADD COLUMN record_ts DATE NOT NULL DEFAULT now();

ALTER TABLE kdata.visit
ADD COLUMN record_ts DATE NOT NULL DEFAULT now();

ALTER TABLE kdata.service
ADD COLUMN record_ts DATE NOT NULL DEFAULT now();

ALTER TABLE kdata.relationship
ADD COLUMN record_ts DATE NOT NULL DEFAULT now();

ALTER TABLE kdata.payment_type
ADD COLUMN record_ts DATE NOT NULL DEFAULT now();

ALTER TABLE kdata.payments
ADD COLUMN record_ts DATE NOT NULL DEFAULT now();



